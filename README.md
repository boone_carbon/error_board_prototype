# Error Board Prototype
This little piece of code demos how to listen for robot state.
  
## Usage
1. Open the HTML file in a web browser. No web server required!
2. Open a command line to play with ROS (the web page explains in detail)
